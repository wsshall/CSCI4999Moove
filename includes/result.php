<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/bootbox/4.4.0/bootbox.min.js"></script>
<script type="text/javascript" src="/deleteRecords.js"></script>

<article class="search-result row">
    <div class="col-xs-12 col-sm-12 col-md-2">
        <ul class="meta-search">
            <li><span><?php echo $rdate; ?></span></li>
            <li><span><?php echo $rtime; ?></span></li>
            <li><span>Seats Available - <?php echo $rseats; ?></span></li>
            <li>Vehicle : <?php echo $rvehicle ?></li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="map"></div>

    </div>
    <div class="col-xs-12 col-sm-12 col-md-7">
        <ul>
            <li><?php echo $rorigin ?> to <?php echo $rdestination; ?>
            </li>
            <li>
                <form name="sendtopayment" action="/waypoint.php" method="post">
                    <input type="hidden" name="remail" value="<?php echo $remail; ?>">
                    <input type="hidden" id="rorigin" name="rorigin" value="<?php echo $rorigin; ?>">
                    <input type="hidden" id="rdestination" name="rdestination" value="<?php echo $rdestination; ?>">
                    <input type="hidden" name="rdate" value="<?php echo $rdate; ?>">
                    <input type="hidden" name="rtime" value="<?php echo $rtime; ?>">
                    <button type="submit" class="btn btn-primary">View Map</button>
                </form>
                <br>
                <button type="button" class="btn btn-success"><a href="/payment.php" title="Click here to pay and book the ride" class="nav-link">Accept</a></button>
                <!--<button type="button" class="btn btn-danger"> <a href="deleterequestedride.php?id=7"> Reject</a></button> -->

                <?php
                $sql = "SELECT id, origin, destination FROM requestedrides ";
                $resultset = mysqli_query($conn, $sql) or die("database error:". mysqli_error($conn));

                $deleteButtonPrinted = false;

                    while ($rows = mysqli_fetch_assoc($resultset) && $deleteButtonPrinted == false) {
                        ?>
                                <a class="delete_employee" data-emp-id="<?php echo $rows["id}"]; ?>" href="javascript:void(0)">
                                    <i class="btn btn-danger"> Delete</i>
                                </a>
                        <?php

                        $deleteButtonPrinted = true;
                }
                ?>

            </li>
        </ul>

    </div>
</article>


<script>
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: {lat: 41.85, lng: -87.65}
        });
        directionsDisplay.setMap(map);

        // document.getElementById('submit').addEventListener('click', function() {
        //     calculateAndDisplayRoute(directionsService, directionsDisplay);
        // });
        window.onload = calculateAndDisplayRoute(directionsService, directionsDisplay);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var waypts = [];
        // var checkboxArray = document.getElementById('waypoints');
        // for (var i = 0; i < checkboxArray.length; i++) {
        //     if (checkboxArray.options[i].selected) {
        //         waypts.push({
        //             location: checkboxArray[i].value,
        //             stopover: true
        //         });
        //     }
        // }

        directionsService.route({
            origin: document.getElementById('rorigin').value,
            destination: document.getElementById('rdestination').value,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, function(response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions-panel');
                summaryPanel.innerHTML = '';
                // For each route, display summary information.
                for (var i = 0; i < route.legs.length; i++) {
                    var routeSegment = i + 1;
                    summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment +
                        '</b><br>';
                    summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                    summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                    summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                }
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<!--<script async defer-->
<!--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBM9gSZW7vdeBC14STP8fAhaMDg0MWqR6s&callback=initMap">-->
<!--</script>-->
<!--<script async defer-->
<!--        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACv_02IJei64-SGTwF-AAPoj2_lnaPRc8&callback=initMap">-->
<!--</script>-->

